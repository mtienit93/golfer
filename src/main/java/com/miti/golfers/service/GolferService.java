package com.miti.golfers.service;

import com.miti.golfers.dto.GolferDto;
import com.miti.golfers.mapper.GolferMapper;
import com.miti.golfers.model.Golfer;
import com.miti.golfers.repository.GolferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class GolferService {

    private GolferMapper mapper = GolferMapper.INSTANCE;

    @Autowired
    private GolferRepository repository;

    public GolferDto createGolfer(GolferDto dto) {
        Golfer golfer = mapper.toEntity(dto);
        return mapper.toDto(repository.save(golfer));
    }

    public Page<GolferDto> getAllGolfers(Pageable pageable) {
        return repository.findAll(pageable).map(mapper::toDto);
    }
}
