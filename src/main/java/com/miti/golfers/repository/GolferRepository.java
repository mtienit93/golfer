package com.miti.golfers.repository;

import com.miti.golfers.model.Golfer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GolferRepository extends JpaRepository<Golfer, Long> {
}
