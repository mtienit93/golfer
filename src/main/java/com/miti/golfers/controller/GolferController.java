package com.miti.golfers.controller;

import com.miti.golfers.dto.GolferDto;
import com.miti.golfers.feign.CourseFeignClient;
import com.miti.golfers.service.GolferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping(value = "api/v1/golfers")
public class GolferController {

    @Autowired
    private GolferService service;

    @Autowired
    private CourseFeignClient courseFeignClient;

    @PostMapping
    public GolferDto create(@Valid @RequestBody GolferDto golfer){
        return service.createGolfer(golfer);
    }

    @GetMapping
    public Page<GolferDto> getAllGolfers(Pageable pageable) {
        return service.getAllGolfers(pageable);
    }

    @GetMapping("/test")
    public String test(@RequestHeader Map<String, String> headers) {
        return headers.toString();
    }


}
