package com.miti.golfers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;


@EnableFeignClients
@SpringBootApplication
public class GolfersApplication {

	public static void main(String[] args) {
		SpringApplication.run(GolfersApplication.class, args);
	}

}
