package com.miti.golfers.mapper;

import com.miti.golfers.dto.GolferDto;
import com.miti.golfers.model.Golfer;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface GolferMapper {

    GolferMapper INSTANCE = Mappers.getMapper(GolferMapper.class);

    Golfer toEntity(GolferDto dto);
    GolferDto toDto(Golfer golfer);
}
