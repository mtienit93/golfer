package com.miti.golfers.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class GolferDto {

    private Long id;
}
