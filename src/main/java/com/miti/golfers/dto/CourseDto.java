package com.miti.golfers.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
public class CourseDto {

    private Long id;

    @NotNull(message = "Image could not be null")
    private String image;

    @NotNull(message = "Course Name could not be null")
    private String courseName;

    private String description;

    private Float rating;

    @NotNull(message = "Hole could not be null")
    private Integer hole;

    private String review;

    @NotNull(message = "Location could not be null")
    private String location;
}
