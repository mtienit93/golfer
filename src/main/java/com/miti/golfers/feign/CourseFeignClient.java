package com.miti.golfers.feign;

import com.miti.golfers.dto.CourseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("course-service")
public interface CourseFeignClient {

    @GetMapping(value = "api/v1/courses/{courseName}")
    CourseDto getCourse(@PathVariable("courseName") String courseName);
}
